// create function logReceipt that accepts menu items (1 or many) as objects
// with these properties: {descr, price}
// i.e. {descr: 'Coke', price: 1.99}
// function should log each item to the console and log a total price
console.log(`Question 1: ItemizedReciept`);
const logReceipt = (...items) => {
	let total = 0;
	items.forEach((items) => {
		total += items.price;
		console.log(`${items.descr} -  $${items.price}`);
	});
	console.log(`Total - $${total}`);
};

// Check
logReceipt(
	{ descr: 'Burrito', price: 5.99 },
	{ descr: 'Chips & Salsa', price: 2.99 },
	{ descr: 'Sprite', price: 1.99 },
	{ descr: `Brownie`, price: 11.99 }
);
// should log something like:
// Burrito - $5.99
// Chips & Salsa - $2.99
// Sprite - $1.99
