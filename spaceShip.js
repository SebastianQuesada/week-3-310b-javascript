// 1. Create a class function SpaceShip
// - should set two properties: name and topSpeed
// - should have a method accelerate that logs to the console
//   `${name} moving to ${topSpeed}`
console.log('Question 1. SpaceShip');
class Spaceship {
	constructor(_name, _topSpeed) {
		(this._name = _name), (this._topSpeed = _topSpeed);
	}
	acceleration() {
		const { _name, _topSpeed } = this;
		console.log(`${_name} is traveling at ${_topSpeed}`);
	}
}
console.log('____________');

// 2. Call the constructor with a couple ships,
// and call accelerate on both of them.
console.log('Question 2.');
USSKittyHawk = new Spaceship(`USS KittyHawk`, `25050 kts`);
USSCarl_Vinson = new Spaceship(`USS Carl Vinson`, `38465 kts`);
USSKittyHawk.acceleration();
USSCarl_Vinson.acceleration();
console.log('____________');
